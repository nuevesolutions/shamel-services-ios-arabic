//
//  SAWaveToast.swift
//  SAWaveToast
//
//  Created by Taiki Suzuki on 2015/08/24.
//
//

import UIKit

public class SAWaveToast: UIViewController {

    static let Spaces = UIEdgeInsets(top: 10, left: 10, bottom: 5, right: 10)
    static let ExtraSpace: CGFloat = 5
    
    let containerView: UIView = UIView()
    let waveView: SAWaveView = SAWaveView()
    let contentView: UIView = UIView()
    let textView: UITextView = UITextView()
    var waveColor: UIColor = .cyan
    var attributedText: NSMutableAttributedString = NSMutableAttributedString()
    var stopAnimation: Bool = false
    var duration: TimeInterval = 5
    
    //Constrants
    var containerViewBottomConstraint: NSLayoutConstraint?
    var textViewCenterXConstraint: NSLayoutConstraint?
    
    public convenience init(text: String, font: UIFont? = nil, fontColor: UIColor? = nil, waveColor: UIColor? = nil, duration: TimeInterval? = nil) {
        var attributes: [String : AnyObject] = [String : AnyObject]()
        if let font = font {
            attributes[NSFontAttributeName] = font
        }
        if let fontColor = fontColor {
            attributes[NSForegroundColorAttributeName] = fontColor
        }
        self.init(attributedText: NSAttributedString(string: text, attributes: attributes), waveColor: waveColor, duration: duration)
    }
    
    public init(attributedText: NSAttributedString, waveColor: UIColor? = nil, duration: TimeInterval? = nil) {
        super.init(nibName: nil, bundle: nil)
        if let waveColor = waveColor {
            self.waveColor = waveColor
        }
        if let duration = duration {
            self.duration = duration
        }
        self.attributedText.append(attributedText)
        waveView.color = self.waveColor
        
        switch UIDevice.current.systemVersion.compare("8.0.0", options: NSString.CompareOptions.numeric) {
            case .orderedSame , .orderedDescending:
                providesPresentationContextTransitionStyle = true
                definesPresentationContext = true
                modalPresentationStyle = .overCurrentContext
            case .orderedAscending:
                modalPresentationStyle = .currentContext
        }
    }
    
    required public init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {}
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .clear
        
        let width = UIScreen.main.bounds.size.width - SAWaveToast.Spaces.left + SAWaveToast.Spaces.right
        let textHeight = attributedText.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).size.height
        
        setContainerView(textHeight: textHeight)
        setWaveView(textHeight: textHeight)
        setContentView(textHeight: textHeight)
        setTextView(textHeight: textHeight)
    }

    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        waveView.startAnimation()
        floatingAnimation()
        
        containerViewBottomConstraint?.constant = 0
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            self.containerView.layoutIfNeeded()
        }, completion: nil)
        
        let delay = duration * Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            self.stopAnimation = true
        }
       
    }
    
    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK: - Private Methods
extension SAWaveToast {
    func setContainerView(textHeight: CGFloat) {
        view.addSubview(containerView)
        containerView.backgroundColor = .clear
        containerView.translatesAutoresizingMaskIntoConstraints  = false
        let height = textHeight + SAWaveView.Height + SAWaveToast.Spaces.top + SAWaveToast.Spaces.bottom + SAWaveToast.ExtraSpace
        let bottomConstraint = NSLayoutConstraint(item: containerView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant:height)
        view.addConstraints([
            NSLayoutConstraint(item: containerView, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 0),
            bottomConstraint,
            NSLayoutConstraint(item: containerView, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: containerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: height)
        ])
        containerViewBottomConstraint = bottomConstraint
    }
    
    func setWaveView(textHeight: CGFloat) {
        containerView.addSubview(waveView)
        waveView.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints([
            NSLayoutConstraint(item: waveView, attribute: .left, relatedBy: .equal, toItem: containerView, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: waveView, attribute: .top, relatedBy: .equal, toItem: containerView, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: waveView, attribute: .right, relatedBy: .equal, toItem: containerView, attribute: .right, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: waveView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: SAWaveView.Height)
        ])
    }
    
    func setContentView(textHeight: CGFloat) {
        containerView.addSubview(contentView)
        contentView.backgroundColor = waveColor
        contentView.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints([
            NSLayoutConstraint(item: contentView, attribute: .left, relatedBy: .equal, toItem: containerView, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: contentView, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: contentView, attribute: .right, relatedBy: .equal, toItem: containerView, attribute: .right, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: contentView, attribute: .top, relatedBy: .equal, toItem: waveView, attribute: .bottom, multiplier: 1, constant: 0)
        ])
    }
    
    func setTextView(textHeight: CGFloat) {
        contentView.addSubview(textView)
        textView.backgroundColor = .clear
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isUserInteractionEnabled = false
        textView.contentInset = UIEdgeInsets(top: -10, left: -4, bottom: 0, right: 0)
        let width = UIScreen.main.bounds.size.width - (SAWaveToast.Spaces.right + SAWaveToast.Spaces.left)
        let centerXConstraint = NSLayoutConstraint(item: textView, attribute: .centerX, relatedBy: .equal, toItem: contentView, attribute: .centerX, multiplier: 1, constant: 0)
        contentView.addConstraints([
            centerXConstraint,
            NSLayoutConstraint(item: textView, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1, constant: -(SAWaveToast.Spaces.bottom + SAWaveToast.ExtraSpace)),
            NSLayoutConstraint(item: textView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: width),
            NSLayoutConstraint(item: textView, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: SAWaveToast.Spaces.top)
        ])
        textViewCenterXConstraint = centerXConstraint
        textView.attributedText = attributedText
    }
    
    func willDisappearToast() {
        containerViewBottomConstraint?.constant = containerView.frame.size.height
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
            self.containerView.layoutIfNeeded()
        }) { finished in
            self.dismiss(animated : false, completion: nil)
        }
    }
    
    func floatingAnimation() {
        stopAnimation = false
        let totalValue = (SAWaveToast.Spaces.left + SAWaveToast.Spaces.right) / 4
        let delta = (CGFloat(arc4random_uniform(UINT32_MAX)) / CGFloat(UINT32_MAX) ) * totalValue
        textViewCenterXConstraint?.constant = (arc4random_uniform(UINT32_MAX) % 2 == 0) ? delta : -delta
        if containerViewBottomConstraint?.constant == 0 {
            containerViewBottomConstraint?.constant = SAWaveToast.ExtraSpace
        } else {
            containerViewBottomConstraint?.constant = 0
        }
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.containerView.layoutIfNeeded()
        }) { finished in
            if self.stopAnimation {
                self.willDisappearToast()
            } else {
                self.floatingAnimation()
            }
        }
    }
}
